import React from 'react'
import {LogoJ,IconFacebook,IconLinkedIn,IconDiscord, IconInstagram, IconGithub} from '../../../assets'
import './footer.scss'


const Icon = ({img}) => {
    return (
        <div className="icon-wrapper">
            <img className="icon-medsoc" src={img} alt="icon"/>
        </div>
    )
}

const Footer = () => {
    return (
        <div >
            <div className="footer">
                <div>
                    <img className="logo" src={LogoJ} alt="logo" />
                    <p>Fauzi Coding</p>
                </div>
                <div className="social-wrapper">
                    <Icon img={IconFacebook} />
                    <Icon img={IconLinkedIn} />
                    <Icon img={IconGithub} />
                    <Icon img={IconInstagram} />
                    <Icon img={IconDiscord} />
                </div>
            </div>
            <div className="copyright">
                <p>Copyright &#169; 2020 - 2021 Fauzi Coding All Rights Reserved.</p>
            </div>
        </div>
    )
}

export default Footer
