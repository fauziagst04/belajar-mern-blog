import React from 'react'
import {RegisterBg} from '../../../assets'
import './BlogItem.scss'

const BlogItem = () => {
    return (
        <div className="blog-item">
            <img className="image-thumb" src={RegisterBg} alt="post"/>
            <div className="content-detail">
                <p className="title">Title Blog</p>
                <p className="author">Author - Date Post</p>
                <p className="body">Cillum enim labore adipisicing velit culpa occaecat magna nulla aliquip. Reprehenderit eiusmod proident amet incididunt incididunt velit ullamco aute nostrud dolore excepteur commodo. Adipisicing amet nisi tempor consectetur. Voluptate ipsum sint nisi officia minim cillum laborum cupidatat nulla Lorem nisi amet.</p> 
            </div>

        </div>
    )
}

export default BlogItem