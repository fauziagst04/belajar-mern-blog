import React from "react";
import { Input, Button, Upload } from "../../components";

const CreateBlog = () => {
  return (
    <div>
      <p>Content Create Blog</p>
      <Input label="Post Title" />
      <Upload />
      <textarea></textarea>
      <Button title="Save" />
    </div>
  );
};

export default CreateBlog;
