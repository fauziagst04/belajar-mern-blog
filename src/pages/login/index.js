import React from 'react'
import {LoginBg} from '../../assets'; 
import {Input, Button, Gap, Link} from '../../components'

const Login = () => {
    return (
        <div className="main-page">
            <div className="left">
                <img src={LoginBg} className="bg-images" alt="imageBg"/>
            </div>
            <div className="right">
                <p>Login</p>
                <Input label="Email" placeholder="Email"/>
                <Gap height={18} />
                <Input label="Password" placeholder="Password"/>
                <Gap height={40} />
                <Button title="Login" />
                <Gap height={80} />
                <Link title="Don't have a account ?, click here"/>
            </div>
        </div>
    )
}

export default Login
