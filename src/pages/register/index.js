import React from 'react'
import './register.scss'
import {RegisterBg} from '../../assets'; 
import {Input, Button, Gap, Link} from '../../components'

const Register = () => {
    return (
        <div className="main-page">
            <div className="left">
                <img src={RegisterBg} className="bg-images" alt="imageBg"/>
            </div>
            <div className="right">
                <p>Register</p>
                <Input label="Full Name" placeholder="Full Name"/>
                <Gap height={18} />
                <Input label="Email" placeholder="Email"/>
                <Gap height={18} />
                <Input label="Password" placeholder="Password"/>
                <Gap height={40} />
                <Button title="Register" />
                <Gap height={80} />
                <Link title="Go Back Login"/>
            </div>
        </div>
    )
}

export default Register;
