import RegisterBg from './images/register-bg.jpg';
import LoginBg from './images/login-bg.jpg'; 
import LogoJ from './images/logo-j.png';
//icon
import IconFacebook from './icon/facebook.svg'
import IconGithub from './icon/github.svg'
import IconLinkedIn from './icon/linkedin.svg'
import IconDiscord from './icon/discord.svg'
import IconInstagram from './icon/instagram.svg'


export{RegisterBg, LoginBg, LogoJ,IconFacebook, IconGithub, IconDiscord,IconLinkedIn,IconInstagram};